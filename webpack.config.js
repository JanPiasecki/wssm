/**
 * Created by PiaseckiJ on 07/04/2017.
 */
module.exports = {

    entry: "./src/app.js",
    output: {
        filename: "./build/bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.css$/, // Only .css files
                loader: 'style-loader!css-loader' // Run both loaders
            },
        ],

    }
}
