/**
 * Created by PiaseckiJ on 07/04/2017.
 */
import './style.css';

(function() {

    console.log('hello from angular');

    var app = angular.module('app', []);

    var MainController = function($scope, $interval, $http ,$event) {

        console.log('hello from MainController');

        $scope.port = 8000;
        $scope.simulations = [];
        $scope.uniqueSimulations = [];



        var  getUniqueSimulations = function() {

            var seen ={};

            if($scope.simulations.length <1)
            {
                return [];
            }

            var uniqueSims = $scope.simulations.filter(function(sim){
                if(seen.hasOwnProperty(sim.name)){
                    return false;
                }else{
                    seen[sim.name] = true;
                    return true;
                }
            });

            $scope.uniqueSimulations = uniqueSims;
        };

        var getData = function() {

            var lastReq = 0;

            var getCall = 0;

            $interval(function() {

                getCall++;

                console.log('All sims no '+ $scope.simulations.length + ' getCall '+ getCall);

                if($scope.simulations !==null && $scope.simulations.length>0){

                    lastReq = $scope.simulations[$scope.simulations.length-1].requestId;
                }

                console.log('lastReq ' + typeof lastReq + ' ' + lastReq);

                $http.get('http://localhost:' + $scope.port,{
                    params:{
                        lastReq: lastReq
                    }
                })
                    .then(function(response)
                    {
                        var freshSims = response.data.simulations;

                        console.log('freshSims');
                        console.log(freshSims);
                        console.log('1------------------------------------------------');

                        if(freshSims !==null && freshSims.length>0){
                            var updatedSims = $scope.simulations;

                            console.log('old sims');
                            console.log(updatedSims);
                            console.log('2------------------------------------------------');


                            updatedSims = updatedSims.concat(freshSims);
                            $scope.simulations = updatedSims;

                            console.log('updated sims');
                            console.log($scope.simulations);
                            console.log('3------------------------------------------------');
                            console.log(freshSims.length + ' simulations added' );
                        }

                        getUniqueSimulations();

                    },function(error)
                    {
                        console.log(error.status);
                    });

                console.log('All sims');
                console.log($scope.simulations);
                console.log('4-----------------------------------------------');

            },5000);
        };

        getData();

        console.log('---END---');

        var highlightedSimId = 0;

        $scope.setHighlightedSimId = function(simulationId) {
            highlightedSimId = simulationId;
        }

        $scope.isHighlighted = function(simulation) {
            return highlightedSimId === simulation.id;
        }

        /*

        var port = 3000;
        express.listen(port, function(){
            console.log('Running on PORT: ' + port);
        });

        var requestsCounter = 0;

        var genSimulations = function(simCount){

            var simNo = Math.floor(Math.random() * simCount) + 1;

            var method;

            if((Math.floor(Math.random() * 2))==0){
                method = "get";

            }else{
                method = "post";
            }
            requestsCounter++;
            $scope.simulations.push(
                {
                    "requestId": requestsCounter,
                    "timestamp": Date.now(),
                    "id": simNo,
                    "name": "simulation"+simNo,
                    "url": "publish"+simNo,
                    "method": method,
                    "response": "hello from simulation "+ simNo +" - response "+ requestsCounter
                }
            );

            getUniqueSimulations();
        }

        var generator = function(){

            var simCount = 9;
            var responseCount = 30;

            $interval(genSimulations, 600, 30, true,simCount);
            /*
            for(var i = 0; i<responseCount ; i++){

                var delay = Math.floor(Math.random() * 4000) + 1000;

                console.log("interval delay "+ delay);

                $interval(genSimulations, delay, 1, true,simCount);
            }

        };
        //generator();*/
    };

    app.controller("MainController", ["$scope","$interval","$http", MainController]);

}());